<div class="w-full px-3 pb-6 lg:max-w-6xl lg:mx-auto">
    <div class="mb-20">
        <div class="text-3xl text-gray-900 pb-12 flex justify-center">
            <span class="ml-3 font-bold"></span>
        </div>
        <div class="grid gap-6 grid-cols-1 sm:grid-cols-4">
            <a href=""
               class="block p-5 rounded-lg bg-white hover:shadow-lg course-item group text-center">
                <div>
                    <img src="" class="object-cover rounded-lg inline-block" width="120"
                         height="160">
                </div>
                <div class="mt-3 text-base text-gray-800 font-bold">

                </div>
                <div class="mt-3">

                    <span class="text-red-500 font-medium text-base"><small>￥</small></span>

                    <span class="text-green-500 font-medium text-sm">免费</span>

                </div>
            </a>
        </div>
    </div>
</div>